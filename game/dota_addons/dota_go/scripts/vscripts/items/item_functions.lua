function DropItemOnDeath(keys)
  local killedUnit = EntIndexToHScript(keys.caster_entindex)
  local itemName = tostring(keys.ability:GetAbilityName())
  if killedUnit:IsHero() or killedUnit:HasInventory() then
    for itemSlot = 0, 5, 1 do
      if killedUnit ~= nil then
        local Item = killedUnit:GetItemInSlot(itemSlot)
        if Item ~= nil and Item:GetName() == itemName then
          local newItem = CreateItem(itemName, nil, nil)
          CreateItemOnPositionSync(killedUnit:GetOrigin(), newItem)
          killedUnit:RemoveItem(Item)
        end
      end
    end
  end
end
