"use strict";

function OnWizardSelectPressed() {
  var playerInfo = Game.GetLocalPlayerInfo();
  if (!playerInfo)
    return;

  GameEvents.SendCustomGameEventToServer("CustomHeroSelection_WizardSelected", {playerID: playerInfo.player_id})
}

function OnWarriorSelectPressed() {
  var playerInfo = Game.GetLocalPlayerInfo();
  if (!playerInfo)
    return;

  GameEvents.SendCustomGameEventToServer("CustomHeroSelection_WarriorSelected", {playerID: playerInfo.player_id})
}

function OnWizardSelected(event_data) {
  $('#WizardSelectButton').AddClass('Selected')
}

function OnCustomHeroSelectorShow(event_data) {
  $('#HeroSelector').visible(true)
}

function OnCustomHeroSelectorHide(event_data) {
  $('#HeroSelector').visible(false)
}

GameEvents.Subscribe("WizardSelected", OnWizardSelected);
GameEvents.Subscribe("CustomHeroSelectorShow", OnCustomHeroSelectorShow)
GameEvents.Subscribe("CustomHeroSelectorHide", OnCustomHeroSelectorHide)
