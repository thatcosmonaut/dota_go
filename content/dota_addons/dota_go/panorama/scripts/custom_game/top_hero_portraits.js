function AddHero(msg) {
  var team = msg.team;
  var heroname = msg.heroname;
  var index = msg.nthIndex;

  if (team === 2){
    AddRadiantHero(heroname, index);
  } else if (team === 3) {
    AddDireHero(heroname, index);
  }
}

function AddRadiantHero(heroname, index) {
  var id = '#RadiantHero' + index;
  var panel = $(id);
  panel.heroname = heroname;
}

function AddDireHero(heroname, index) {
  var id = '#DireHero' + index;
  var panel = $(id);
  panel.heroname = heroname;
}

(function () {
  GameEvents.Subscribe("hero_spawned", AddHero );
})();
