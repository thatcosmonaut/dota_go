function SetRadiantScore(msg)
{
  $('#RadiantScore').text = msg.score;
}

function SetDireScore(msg)
{
  $('#DireScore').text = msg.score;
}

(function () {
  GameEvents.Subscribe( "radiant_win_round", SetRadiantScore );
  GameEvents.Subscribe( "dire_win_round", SetDireScore );
})();
